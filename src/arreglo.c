#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char ** crear_arreglo(int filas, int columnas){
	char **arreglo = (char **)malloc(filas*sizeof(char*));
	for(int i=0; i<filas; i++){
		*(arreglo+i)= (char *)malloc(columnas);
		memset(*(arreglo+i),0,columnas);
	}

	return arreglo;

}
