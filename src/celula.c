#include <stdio.h>
#include "arreglo.h"

void celula_v_o_m(char **arreglo, char **array, int row, int column, int total_row, int total_column);

void copiar_arreglo(char **arreglo, char **array, int filas, int columnas){
	for(int i=0;i<filas;i++){
		for(int j=0;j < columnas; j++){
			if(arreglo[i][j]==1){
				array[i][j]=1;
			}
		}
	}
}



char ** reglas_celula(char **arreglo, int filas, int columnas){
	char **array = crear_arreglo(filas,columnas);
	copiar_arreglo(arreglo,array,filas,columnas);
	for(int i=0;i<filas;i++){
		for(int j=0;j < columnas; j++){
			celula_v_o_m(arreglo,array,i,j,filas,columnas);
		}
	}
	return array;
}

void celula_v_o_m(char **arreglo, char **array, int row, int column, int total_row, int total_column){

		int suma=0;
		int inicioF =row-1;
		int finF = row+2;
		int inicioC =column-1;
		int finC = column+2;
		if(column==0){
			inicioC = 0;
		}
		if(row==0){
			inicioF = 0;
		}
		if(row==total_row-1){
			finF = row+1;
		}
		if(column==total_column-1){
			finC = column+1;
		}
		for(int m=inicioF; m<finF;m++){
			for(int n=inicioC; n< finC; n++){
				if(m!=row || n!=column){
					suma+=arreglo[m][n];
				}
			}
		}

		if(arreglo[row][column]==0 && suma==3){
			array[row][column]=1;
		}
		if(arreglo[row][column]==1 && suma<=1){
			array[row][column]=0;
		}

		if(arreglo[row][column]==1 && suma>=4){
			array[row][column]=0;
		}
		if(arreglo[row][column]==1 && suma==2){
			array[row][column]=1;
		}
		if(arreglo[row][column]==1 && suma==3){
			array[row][column]=1;
		}

}

