#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "util.h"
#include "vida.h"
#include "celula.h"

void resultadoGeneracion(int generacion, int vivasG, int muertasG, int vivasTotal, int muertasTotal, int promVivas, int promMuertas){
    printf("Número de generación: %d\n", generacion );
    printf("   Número de células vivas en esta generación:  %d\n", vivasG);
    printf("   Número de células muertas en esta generación:  %d\n", muertasG );
    printf("   Número de células vivas desde generación 0:  %d\n", vivasTotal );
    printf("   Número de células muertas desde generación 0:  %d\n", muertasTotal );
	printf("   Promedio final de células vivas por generación:  %d\n", promVivas);
	printf("   Promedio final de células muertas por generación:  %d\n", promMuertas);
}

int main(int argc, char *argv[]){
	int filas = 0;
	int columnas = 0 ;
	int generaciones =0;
	int tiempo = 0;
	int celulaInicial = 0;
    int opt;
	while ((opt = getopt(argc, argv, "f:c:g:s:i:")) != -1) {
        switch (opt) {
               case 'f':
                   filas = atoi(optarg);
                  break;
				  case 'c':
                  columnas = atoi(optarg);
                  break;
				  case 'g':
                   generaciones = atoi(optarg);
                  break;
				  case 's':
                   tiempo = atoi(optarg);
                  break;
				  case 'i':
                   celulaInicial = atoi(optarg);
                  break;
               default: /* '?' */
                   fprintf(stderr, "Options requires an argument.\n");
                   exit(EXIT_FAILURE);

    }}
        int vivasTotal= celulaInicial;
	int muertasTotal = (filas*columnas)-vivasTotal;

	char **arreglo = (char **)malloc(filas * sizeof(char *));
	if(arreglo==NULL){
		return -1;
	}

	for(int i=0;i<filas;i++){
		*(arreglo+i) = (char *)malloc(columnas * sizeof(char));
		if(*(arreglo+i)==NULL){
			return -1;
		}
	}

	llenar_matriz_azar(arreglo,filas,columnas,celulaInicial);

	juego_de_vida newJuego = {.tablero=arreglo, .filas=filas, .columnas=columnas};

	for(int generacion=0; generacion<generaciones+1; generacion++){
                int vivasG=0 ;
	        int muertasG= 0;
		int promVivas = vivasTotal  / generaciones;
		int promMuertas = muertasTotal  / generaciones;


		dibujar_grilla(newJuego.tablero, filas, columnas);
		if(generacion==0){
			vivasG =celulaInicial;
			muertasG = (filas*columnas)-vivasG;
			promVivas=0;
			promMuertas=0;
		}else{
			for(int i=0;i<filas;i++){
		      		for(int j=0; j<columnas;j++){
                             		 if(newJuego.tablero[i][j]==1){
		                     		vivasTotal++;
                                      		vivasG ++;
	                      		}
			        }
			}
                muertasG = (filas*columnas)-vivasG;
                muertasTotal = muertasTotal + muertasG ;
		}

                resultadoGeneracion( generacion, vivasG, muertasG, vivasTotal, muertasTotal, promVivas, promMuertas);

		char **array = reglas_celula(newJuego.tablero, filas, columnas);
		for(int i=0;i<filas;i++){
			free(*(newJuego.tablero+i));
		}
		free(newJuego.tablero);
		newJuego.tablero=array;


		usleep(tiempo);
	}
	return 0;

}
