bin/vida: obj/main.o obj/celula.o obj/util.o obj/arreglo.o
	gcc -Wall -fsanitize=address,undefined $^ -o bin/vida

obj/main.o: src/main.c
	gcc -Wall -c -I include/ src/main.c -o obj/main.o

obj/celula.o: src/celula.c
	gcc -Wall -c -I include/ $^ -o obj/celula.o

obj/util.o: src/util.c
	gcc -Wall -c src/util.c -o obj/util.o

obj/arreglo.o: src/arreglo.c
	gcc -Wall -c $^ -o $@

clean:
	rm -rf obj bin
	mkdir obj bin
